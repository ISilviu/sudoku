﻿# Sudoku game	
An eye-candy implementation of Sudoku done in C# and WPF. Briefly, it has the following features:

 - users with image profile
 - the base game of Sudoku
 - besides the 9x9 standard board, the user can also select boards of the following sizes: 4x4, 6x6, 8x8
 - save/load game 
 - timer
 - user stats -  [games started, games won]

Technical aspects:
- saving/loading data from the disk is done asynchronously
- the project intensively uses data-binding to provide a rich user interface

Some screenshots from the game:
![enter image description here](https://i.postimg.cc/J7Dqg3XY/sudoku4.png)
![enter image description here](https://i.postimg.cc/YCNQ7hnF/sudoku.png)
![enter image description here](https://i.postimg.cc/3RzPTSNn/sudoku2.png)![enter image description here](https://i.postimg.cc/pXSntKJy/sudoku3.png)

