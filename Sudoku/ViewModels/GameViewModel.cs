﻿using Sudoku.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Sudoku.ViewModels
{
    public sealed class GameViewModel : INotifyPropertyChanged
    {
        private static readonly int defaultBoardSize = 9;

        public static readonly Tuple<int, int> fourByFour = new Tuple<int, int>(4, 4);
        public static readonly Tuple<int, int> sixBySix = new Tuple<int, int>(6, 6);
        public static readonly Tuple<int, int> eightByEight = new Tuple<int, int>(8, 8);
        public static readonly Tuple<int, int> nineByNine = new Tuple<int, int>(9, 9);

        private static readonly string sizeTemplate = "Board size: {0} x {1}";

        public static readonly Dictionary<Tuple<int, int>, string> sizeToConfigurationsPath = new Dictionary<Tuple<int, int>, string>
        {
            { fourByFour, "../../Resources/boards/4x4/" },
            { sixBySix, "../../Resources/boards/6x6/" },
            { nineByNine, "../../Resources/boards/9x9/" },
            { eightByEight, "../../Resources/boards/8x8/" }
        };

        public static readonly string saveGamesFolderPath = "../../Resources/saves/";

        private bool isPreviewEnabled = false;
        public bool IsPreviewEnabled
        {
            get => isPreviewEnabled;
            set
            {
                isPreviewEnabled = value;
                AreButtonsEnabled = !isPreviewEnabled;
                OnPropertyChanged(nameof(IsPreviewEnabled));
            }
        }

        private bool areButtonsEnabled = true;
        public bool AreButtonsEnabled
        {
            get => areButtonsEnabled;
            set
            {
                areButtonsEnabled = value;
                OnPropertyChanged(nameof(AreButtonsEnabled));
            }
        }

        private Board previewBoard;
        public Board PreviewBoard
        {
            get => previewBoard;
            set
            {
                previewBoard = value;
                OnPropertyChanged(nameof(PreviewBoard));
            }
        }

        private Board gameBoard;
        public Board GameBoard
        {
            get => gameBoard;
            set
            {
                gameBoard = value;
                OnPropertyChanged(nameof(GameBoard));
            }
        }

        public User CurrentUser { get; }

        private Tuple<int, int> boardSize = new Tuple<int, int>(defaultBoardSize, defaultBoardSize);
        public Tuple<int, int> BoardSize
        {
            get => boardSize;
            set
            {
                boardSize = value;
                BoardSizeString = string.Format(sizeTemplate, boardSize.Item1, boardSize.Item2);
                PreviewBoard = Board.CreatePreviewBoard(boardSize.Item1, boardSize.Item2);
                OnPropertyChanged(nameof(BoardSize));
            }
        }

        private string boardSizeString = string.Format(sizeTemplate, defaultBoardSize, defaultBoardSize);
        public string BoardSizeString
        {
            get => boardSizeString;
            set
            {
                boardSizeString = value;
                OnPropertyChanged(nameof(BoardSizeString));
            }
        }

        public GameViewModel(User user)
        {
            CurrentUser = user;
            PreviewBoard = Board.CreatePreviewBoard(boardSize.Item1, boardSize.Item2);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
