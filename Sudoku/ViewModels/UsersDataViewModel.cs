﻿using Sudoku.Domain;
using Sudoku.Utilities.FileIO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Media.Imaging;

namespace Sudoku.ViewModels
{
    public sealed class UsersDataViewModel : INotifyPropertyChanged
    {
        private static readonly string usersFilePath = "../../Resources/players.json";
        private static readonly string defaultImagesPath = "../../Resources/images/";

        private BitmapImage currentImage;
        public BitmapImage CurrentImage
        {
            get => currentImage;
            set
            {
                currentImage = value;
                OnPropertyChanged(nameof(CurrentImage));
            }
        }

        public int CurrentImageIndex { get; set; }
        public List<BitmapImage> Images { get; } = null;
        public ObservableCollection<User> Users { get; }

        public UsersDataViewModel()
        {
            Users = new ObservableCollection<User>();        
            Users.CollectionChanged += Users_CollectionChanged;
            var users = IOServices.Deserialize<List<User>>(usersFilePath);
            if(users != null)
                users.ForEach(user => Users.Add(user));
           
            IEnumerable<BitmapImage> bitmapImages = null;
            var thread = new Thread(() => { bitmapImages = IOServices.ReadImages(defaultImagesPath); });
            thread.Start();
            thread.Join();
            if (!thread.IsAlive)
            {
                if (bitmapImages != null)
                {
                    Images = bitmapImages.ToList();

                    CurrentImage = (!thread.IsAlive) ? Images.First() : null;
                    CurrentImageIndex = 0;
                }
            }
        }

        private async void Users_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged -= UserPropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged += UserPropertyChanged;
                }
            }
            await IOServices.SerializeAsync(Users, usersFilePath);
        }

        private async void UserPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           await IOServices.SerializeAsync(Users, usersFilePath);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
