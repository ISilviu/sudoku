﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Sudoku.ViewModels
{
    public class UserInputViewModel
    {
        public Tuple<int, int> BoardSize { get; private set; }

        public static readonly Dictionary<Tuple<int, int>, Regex> sizeToAcceptedDigits = new Dictionary<Tuple<int, int>, Regex>
        {
            { GameViewModel.fourByFour,  new Regex(@"^[1-4]{1}$") },
            { GameViewModel.sixBySix, new Regex(@"^[1-6]{1}$") },
            { GameViewModel.nineByNine, new Regex(@"^[1-9]{1}$") },
            { GameViewModel.eightByEight, new Regex(@"^[1-8]{1}$") }
        };

        public UserInputViewModel(Tuple<int, int> boardSize)
        {
            BoardSize = boardSize;
        }
    }
}
