﻿using Sudoku.UI;
using Sudoku.Utilities.FileIO;
using Sudoku.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sudoku.Domain;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

namespace Sudoku
{
    public partial class GameWindow : Window, INotifyPropertyChanged
    {
        private static readonly int defaultUserAllocatedTime = 480;
        private static readonly double defaultTickTime = 1;

        private int userAllocatedTime = defaultUserAllocatedTime;
        private static readonly int timeLimit = 0;
        private static readonly int timeUnit = 60;
        private static readonly Tuple<int, int> defaultBoardSize = new Tuple<int, int>(9, 9);
        private DispatcherTimer timer = new DispatcherTimer();

        private bool isGameRunning = false;
        public bool IsGameRunning
        {
            get => isGameRunning;
            set
            {
                isGameRunning = value;
                OnPropertyChanged(nameof(IsGameRunning));
            }
        }

        public GameWindow(GameViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            MouseDown += Window_MouseDown;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if(userAllocatedTime == timeLimit)
            {
                timer.Stop();

                if(DataContext is GameViewModel viewModel)
                {
                    if (viewModel.GameBoard.EmptyCellsCount == 0 && viewModel.GameBoard.IsValid)
                    {
                        ++viewModel.CurrentUser.GamesWon;
                        MessageBox.Show("You won! Congrats!", "Sudoku", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                    MessageBox.Show("You lost.", "Sudoku", MessageBoxButton.OK, MessageBoxImage.Information);

                ResetState();
                return;
            }
            --userAllocatedTime;
            CountDown.Text = string.Format("0{0}:{1}", userAllocatedTime / timeUnit, userAllocatedTime % timeUnit);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void ExitButton_Clicked(object sender, RoutedEventArgs e) => Close();

        private void AboutButton_Clicked(object sender, RoutedEventArgs e)
        {
            AboutWindow window = new AboutWindow();
            window.ShowDialog();
        }

        private void ConfigureNewTimer()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(defaultTickTime);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private async Task CreateNewGame(Board board = null)
        {
            if (DataContext is GameViewModel viewModel)
            {
                ++viewModel.CurrentUser.GamesStarted;
                IsGameRunning = true;

                if (board == null)
                {
                    int[,] elements = await IOServices.GetRandomBoard(GameViewModel.sizeToConfigurationsPath[viewModel.BoardSize], viewModel.BoardSize);
                    if (elements != null)
                    {
                        viewModel.GameBoard = new Board(elements);
                        viewModel.BoardSize = new Tuple<int, int>(elements.GetLength(0), elements.GetLength(1));
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format("A board could not be loaded.\r\nPlease check if the {0} x {1} folder is empty or has any corrupted file.", viewModel.BoardSize.Item1, viewModel.BoardSize.Item2),
                            "Sudoku", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                else
                {
                    viewModel.GameBoard = board;
                    viewModel.BoardSize = new Tuple<int, int>(board.Elements.Count, board.Elements[0].Count);
                }

                ConfigureNewTimer();
            }
        }

        private async void NewGame_Clicked(object sender, RoutedEventArgs e)
        {
            if (IsGameRunning)
                await CheckUserChoice(UserInterruption.Type.NewGame, this);
            else
                await CreateNewGame();                      
        }

        private void StatsButton_Clicked(object sender, RoutedEventArgs e)
        {           
            if(DataContext is GameViewModel viewModel)
            {
                StatsWindow window = new StatsWindow(viewModel);
                if (timer.IsEnabled)
                {
                    timer.Stop();
                    window.Closed += (s, ev) => timer.Start();
                }
                window.ShowDialog();
            }
        }

        private MessageBoxResult GetUserResponse(UserInterruption.Type type)
        {
            return MessageBox.Show(UserInterruption.TypeToMessage[type], nameof(Sudoku),
                MessageBoxButton.YesNo, MessageBoxImage.Question);
        }
        
        private void ResetGameBoard()
        {
            if (DataContext is GameViewModel viewModel)
            {
                for (int i = 0; i < viewModel.GameBoard.Elements.Count; ++i)
                {
                    for (int j = 0; j < viewModel.GameBoard.Elements[i].Count; ++j)
                        viewModel.GameBoard.Elements[i][j] = null;

                    viewModel.GameBoard.Elements[i] = null;
                }
                viewModel.GameBoard.Elements = null;
            }
        }

        private void ResetState()
        {
            IsGameRunning = false;
            timer.Stop();
            CountDown.Text = string.Empty;
            userAllocatedTime = defaultUserAllocatedTime;
            ResetGameBoard();          
        }

        private async Task CheckUserChoice(UserInterruption.Type type, Window window = null)
        {
            switch (type)
            {
                case UserInterruption.Type.BoardSize:
                    {
                        timer.Stop();
                        var result = GetUserResponse(type);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                {
                                    ResetState();
                                    if (DataContext is GameViewModel viewModel)
                                    {
                                        if (window == this)
                                            viewModel.BoardSize = defaultBoardSize;
                                        else if(window != null)
                                            window.ShowDialog();
                                    }
                                    break;
                                }
                            case MessageBoxResult.No:
                                {
                                    timer.Start();
                                    break;
                                }
                        }
                        break;
                    }
                case UserInterruption.Type.OpenSavedGame:
                    {
                        timer.Stop();
                        var result = GetUserResponse(type);
                        switch(result)
                        {
                            case MessageBoxResult.Yes:
                                {
                                    ResetState();
                                    await OpenGame();
                                    break;
                                }
                            case MessageBoxResult.No:
                                {
                                    timer.Start();
                                    break;
                                }
                        }
                        break;
                    }
                case UserInterruption.Type.NewGame:
                    {
                        timer.Stop();
                        var result = GetUserResponse(type);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                {
                                    ResetState();
                                    await CreateNewGame();
                                    break;
                                }
                            case MessageBoxResult.No:
                                {
                                    timer.Start();
                                    break;
                                }
                        }

                        break;
                    }
            }
        }

        private async void StandardSized_Clicked(object sender, RoutedEventArgs e)
        {
            if (IsGameRunning)
                await CheckUserChoice(UserInterruption.Type.BoardSize, this);
            else if(DataContext is GameViewModel viewModel)
                viewModel.BoardSize = defaultBoardSize;
        }

        private async void CustomSized_Clicked(object sender, RoutedEventArgs e)
        {
            if (DataContext is GameViewModel viewModel)
            {
                BoardSizeSelectionWindow window = new BoardSizeSelectionWindow(viewModel);
                if (IsGameRunning)
                    await CheckUserChoice(UserInterruption.Type.BoardSize, window);
                else
                    window.ShowDialog();
            }
        }

        private void BoardSizeCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {           
            if(DataContext is GameViewModel viewModel)
            {
                BoardSizeSelectionWindow window = new BoardSizeSelectionWindow(viewModel);
                window.ShowDialog();
            }
        }

        private async Task OpenGame()
        {
            if (DataContext is GameViewModel viewModel)
            {
                State savedState = await IOServices.DeserializeAsync<State>(string.Format((GameViewModel.saveGamesFolderPath + "{0}.json"), viewModel.CurrentUser.Name));

                if (savedState != null)
                {
                    userAllocatedTime = savedState.UserAllocatedTime;
                    await CreateNewGame(savedState.GameBoard);
                }
                else
                    MessageBox.Show("Save file not found.", "Sudoku",
                        MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private async void OpenGameButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (IsGameRunning)
                await CheckUserChoice(UserInterruption.Type.OpenSavedGame);
            else
            {
                await OpenGame();
            }
        }

        private void BoardPreviewButton_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsGameRunning;
        }

        private async void SaveGameButton_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DataContext is GameViewModel viewModel)
            {
                State state = new State(viewModel.GameBoard, userAllocatedTime);
               await IOServices.SerializeAsync(state, string.Format((GameViewModel.saveGamesFolderPath + "{0}.json"), viewModel.CurrentUser.Name));
            }
        }

        private void SaveGameButton_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsGameRunning;
        }

        private void BoardButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (DataContext is GameViewModel viewModel)
            {
                var userInputViewModel = new UserInputViewModel(viewModel.BoardSize);
                var window = new UserInputWindow(userInputViewModel);
                if (window.ShowDialog() == true)
                {
                    (sender as Button).Content = window.Answer;
                }

                if (viewModel.GameBoard.EmptyCellsCount == 0 && viewModel.GameBoard.IsValid)
                {
                    ++viewModel.CurrentUser.GamesWon;
                    MessageBox.Show("You won! Congrats!", "Sudoku", MessageBoxButton.OK, MessageBoxImage.Information);

                    Thread.Sleep(5000);
                    ResetState();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
