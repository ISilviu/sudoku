﻿using System.Windows;
using System.Windows.Input;

namespace Sudoku.UI
{
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            MouseDown += Window_MouseDown;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void CloseButton_Clicked(object sender, RoutedEventArgs e) => Close();
      
    }
}
