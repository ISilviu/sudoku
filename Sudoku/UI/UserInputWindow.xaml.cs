﻿using Sudoku.ViewModels;
using System.Windows;
using System.Windows.Input;

namespace Sudoku.UI
{
    public partial class UserInputWindow : Window
    {
        public string Answer => UserInput.Text;
        
        public UserInputWindow(UserInputViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            MouseDown += Window_MouseDown;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void CancelButton_Clicked(object sender, RoutedEventArgs e) => Close();

        private void AddContent_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        { 
            if(DataContext is UserInputViewModel viewModel)
                e.CanExecute = UserInputViewModel.sizeToAcceptedDigits[viewModel.BoardSize].IsMatch(UserInput.Text);          
        }

        private void AddContent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
