﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sudoku.UI.Converters
{
    class BoardButtonEnablingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isReadOnly)
                return isReadOnly ? false : true;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isEnabled)
                return isEnabled ? false : true;
            return false;
        }
    }
}
