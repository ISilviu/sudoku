﻿using FontAwesome.Sharp;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Sudoku.UI.Converters
{
    class BoardValidityToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isBoardValid)
                return isBoardValid ? IconChar.Check : IconChar.Exclamation;
            return IconChar.Exclamation;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
