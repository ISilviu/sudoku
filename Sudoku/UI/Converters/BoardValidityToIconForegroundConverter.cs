﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Sudoku.UI.Converters
{
    class BoardValidityToIconForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isBoardValid)
                return isBoardValid ? new SolidColorBrush(Colors.BlueViolet) : new SolidColorBrush(Colors.Red);
            return new SolidColorBrush(Colors.Red);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
