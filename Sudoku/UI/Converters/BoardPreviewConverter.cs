﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Sudoku.UI.Converters
{
    class BoardPreviewConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isChecked)
                return isChecked ? Visibility.Visible : Visibility.Hidden;
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility visibility)
                return visibility == Visibility.Visible ? true : false;
            return false;
        }
    }
}
