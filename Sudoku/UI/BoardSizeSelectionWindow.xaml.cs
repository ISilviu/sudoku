﻿using Sudoku.ViewModels;
using System.Windows;
using System.Windows.Input;

namespace Sudoku.UI
{
    public partial class BoardSizeSelectionWindow : Window
    {
        public BoardSizeSelectionWindow(GameViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void CloseButton_Clicked(object sender, RoutedEventArgs e) => Close();

        private void FourByFour_Clicked(object sender, RoutedEventArgs e)
        {         
            if(DataContext is GameViewModel viewModel)
            {
                viewModel.BoardSize = GameViewModel.fourByFour;
                Close();
            }
        }

        private void SixBySix_Clicked(object sender, RoutedEventArgs e)
        {            
            if(DataContext is GameViewModel viewModel)
            {
                viewModel.BoardSize = GameViewModel.sixBySix;
                Close();
            }
        }

        private void EightByEight_Clicked(object sender, RoutedEventArgs e)
        {
            if (DataContext is GameViewModel viewModel)
            {
                viewModel.BoardSize = GameViewModel.eightByEight;
                Close();
            }
        }
    }
}
