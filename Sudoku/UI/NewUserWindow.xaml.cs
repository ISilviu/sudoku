﻿using System.Windows;
using System.Windows.Input;
using Sudoku.ViewModels;
using Sudoku.Domain;
using System.Linq;
using System;

namespace Sudoku
{
    public partial class NewUserWindow : Window
    {
        public NewUserWindow(UsersDataViewModel dataContext)
        {
            InitializeComponent();
            MouseDown += Window_MouseDown;
            DataContext = dataContext;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void CancelButton_Clicked(object sender, RoutedEventArgs e) => Close();

        private void OKCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(UsernameField.Text))
            {
                e.CanExecute = false;
                return;
            }

            var viewModel = DataContext as UsersDataViewModel;
            try
            {  
                if (viewModel != null)
                    viewModel.Users.Where(user => user.Name == UsernameField.Text).Single();

               e.CanExecute = false;
            }
            catch(InvalidOperationException)
            {
                e.CanExecute = true;
            }
            catch(ArgumentNullException)
            {
                e.CanExecute = true;
            }
        }

        private void OKCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = DataContext as UsersDataViewModel;
            if (viewModel != null)
            {
                User user = new User(UsernameField.Text, viewModel.CurrentImage.UriSource.OriginalString);
                viewModel.Users.Add(user);
                Close();
            }
        }

        private void ImageLeftCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewModel = DataContext as UsersDataViewModel;

            if(viewModel != null)
                e.CanExecute = viewModel.CurrentImageIndex == 0 ? false : true;
        }

        private void ImageLeftCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = DataContext as UsersDataViewModel;
            if (viewModel != null)
            {
                --viewModel.CurrentImageIndex;
                viewModel.CurrentImage = viewModel.Images[viewModel.CurrentImageIndex];
            }
        }

        private void ImageRightCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewModel = DataContext as UsersDataViewModel;
            if (viewModel != null)
            {
                var lastImageIndex = viewModel.Images.Count - 1;
                e.CanExecute = viewModel.CurrentImageIndex == lastImageIndex ? false : true;
            }
        }

        private void ImageRightCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var viewModel = DataContext as UsersDataViewModel;
            if (viewModel != null)
            {
                ++viewModel.CurrentImageIndex;
                viewModel.CurrentImage = viewModel.Images[viewModel.CurrentImageIndex];                
            }
        }
    }
}
