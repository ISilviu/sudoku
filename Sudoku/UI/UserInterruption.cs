﻿using System.Collections.Generic;

namespace Sudoku.UI
{
    public class UserInterruption
    {
        public enum Type : byte { BoardSize, OpenSavedGame, NewGame };
        public static Dictionary<Type, string> TypeToMessage { get; } = new Dictionary<Type, string>
        {
            {Type.BoardSize, "The game is running. Do you still want to change the board size?\r\n If you choose Yes, the current game will be considered lost." },
            {Type.OpenSavedGame, "The game is running. Do you still want to open a saved game?\r\n If you choose Yes, the current game will be considered lost." },
            {Type.NewGame, "The game is running. Do you still want to create a new game?\r\n If you choose Yes, the current game will be considered lost." }
        };
    }
}
