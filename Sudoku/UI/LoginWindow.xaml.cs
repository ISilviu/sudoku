﻿using Sudoku.Domain;
using Sudoku.Utilities.FileIO;
using Sudoku.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Sudoku
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            MouseDown += Window_MouseDown;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void NewUserCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DataContext is UsersDataViewModel viewModel)
            {
                NewUserWindow window = new NewUserWindow(viewModel);
                window.ShowDialog();
            }
        }

        private void DeleteUserCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var toDeleteName = (AvailableUsers.SelectedItem as User)?.Name;
            if(DataContext is UsersDataViewModel viewModel)
            {
                try
                {
                    IOServices.DeleteFile(string.Format((GameViewModel.saveGamesFolderPath + "{0}.json"), toDeleteName));
                    viewModel.Users.Remove(viewModel.Users.Where(element => element.Name == toDeleteName).Single());
                }
                catch(Exception)
                {
                    viewModel.Users.Where(element => element.Name == toDeleteName).ToList()
                        .ForEach(user => viewModel.Users.Remove(user));
                }
            }
        }

        private void StartGameCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            GameViewModel viewModel = new GameViewModel(AvailableUsers.SelectedItem as User);
            GameWindow window = new GameWindow(viewModel);
            window.ShowDialog();
        }

        private void IsUserSelected(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AvailableUsers.SelectedItem != null ? true : false;
        }

        private void ExitButton_Clicked(object sender, RoutedEventArgs e) => Application.Current.Shutdown();

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var x = 2;
        }
    }
}
