﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace Sudoku.Domain
{
    public class User : INotifyPropertyChanged
    {
        private static readonly int defaultNumberOfGames = 0;

        private string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public BitmapImage Image { get; private set; }

        private int gamesStarted;
        public int GamesStarted
        {
            get => gamesStarted;
            set
            {
                gamesStarted = value;
                OnPropertyChanged(nameof(GamesStarted));
            }
        }

        private int gamesWon;
        public int GamesWon
        {
            get => gamesWon;
            set
            {
                gamesWon = value;
                OnPropertyChanged(nameof(GamesWon));
            }
        }

        private string imagePath;
        public string ImagePath
        {
            get => imagePath;
            set
            {
                imagePath = value;
                Image = new BitmapImage(new Uri(ImagePath, UriKind.Relative));
                OnPropertyChanged(nameof(ImagePath));
            }
        }

        public User(string name, string imagePath)
        {
            Name = name;
            this.imagePath = imagePath;
            try
            {
                Uri uri = new Uri(imagePath, UriKind.Relative);
                Image = new BitmapImage(new Uri(ImagePath, UriKind.Relative));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            gamesStarted = defaultNumberOfGames;
            gamesWon = defaultNumberOfGames;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
