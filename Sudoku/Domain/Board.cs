﻿using Newtonsoft.Json;
using Sudoku.Domain.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Sudoku.Domain
{
    public class Board : INotifyPropertyChanged
    {
        private List<List<bool>> validityMap;

        private bool isValid = false;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                isValid = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public int EmptyCellsCount { get; private set; }

        private ObservableCollection<ObservableCollection<Cell>> elements;
        public ObservableCollection<ObservableCollection<Cell>> Elements
        {
            get => elements;
            set
            {
                elements = value;
                if (elements != null)
                {
                    validityMap = new List<List<bool>>(elements.Count);
                    for (int row = 0; row < elements.Count; ++row)
                    {
                        validityMap.Add(new List<bool>());
                        for (int column = 0; column < elements[row].Count; ++column)
                        {
                            validityMap[row].Add(true);
                            elements[row][column].PropertyChanged += Cell_PropertyChanged;
                        }
                    }
                }
                OnPropertyChanged(nameof(Elements));
            }
        }

        private Board(int rowsCount, int columnsCount)
        {
            Elements = new ObservableCollection<ObservableCollection<Cell>>();

            for (int row = 0; row < rowsCount; ++row)
            {
                Elements.Add(new ObservableCollection<Cell>());
                for (int column = 0; column < columnsCount; ++column)
                {
                    var cell = new Cell(new Tuple<int, int>(row, column), string.Empty, 0, true);
                    cell.PropertyChanged += Cell_PropertyChanged;
                    Elements[row].Add(cell);
                }
            }
        }

        public static Board CreatePreviewBoard(int rowsCount, int columnsCount)
        {
            return new Board(rowsCount, columnsCount);
        }

        [JsonConstructor]
        public Board(ObservableCollection<ObservableCollection<Cell>> cells)
        {
            Elements = cells;           
        }

        public Board(int[,] elements)
        {
            Elements = new ObservableCollection<ObservableCollection<Cell>>();
            validityMap = new List<List<bool>>(elements.GetLength(0));

            for (int row = 0; row < elements.GetLength(0); ++row)
            {
                validityMap.Add(new List<bool>(elements.GetLength(1)));
                Elements.Add(new ObservableCollection<Cell>());
                for (int column = 0; column < elements.GetLength(1); ++column)
                {                   
                    int currentElement = elements[row, column];
                    Cell cell = null;
                    validityMap[row].Add(true);

                    if (currentElement == 0)                   
                        cell = new Cell(new Tuple<int, int>(row, column), string.Empty, currentElement, false);                    
                    else                    
                        cell = new Cell(new Tuple<int, int>(row, column), currentElement.ToString(), currentElement, true);
                                  
                    cell.PropertyChanged += Cell_PropertyChanged;
                    Elements[row].Add(cell);
                }
            }
        }

        private void Cell_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Cell.Text))
            {
                int rowsCount = elements.Count;
                int columnsCount = elements[0].Count;

                if(sender is Cell cell)
                {                    
                    int row = cell.RowAndColumn.Item1;
                    int column = cell.RowAndColumn.Item2;

                    var region = BoardSizeToPositions.values[new Tuple<int, int>(rowsCount, columnsCount)]
                        [new Tuple<int, int>(row, column)];

                    var bounds = BoardSizeToRegions.values[new Tuple<int, int>(rowsCount, columnsCount)]
                        [region];


                    bool wasRegionInvalid = false;
                    bool wasRowInvalid = false;
                    bool wasColumnInvalid = false;

                    cell.Number = int.Parse(cell.Text);

                    for(int i = 0; i < elements.Count; ++i)
                    {
                        if (i == column)
                            continue;

                        if (elements[row][i].Text.Equals(cell.Text))
                        {
                            wasRowInvalid = true;
                            validityMap[row][column] = false;
                            break;
                        }                        
                    }

                    for(int i = 0; i < elements[0].Count; ++i)
                    {
                        if (i == row)
                            continue;
                       
                        if(elements[i][column].Text.Equals(cell.Text))
                        {
                            wasColumnInvalid = true;
                            validityMap[row][column] = false;
                            break;
                        }
                    }

                    for(int i = bounds.Item1.Item1; i <= bounds.Item1.Item2; ++i)
                    {
                        for(int j = bounds.Item2.Item1; j <= bounds.Item2.Item2; ++j)
                        {
                            if (i == row && j == column)
                                continue;

                            if(elements[i][j].Text.Equals(cell.Text))
                            {
                                wasRegionInvalid = true;
                                validityMap[row][column] = false;
                                break;
                            }
                        }
                    }

                    if (!wasColumnInvalid && !wasRowInvalid && !wasRegionInvalid)
                        validityMap[row][column] = true;

                    wasColumnInvalid = false;
                    wasRowInvalid = false;
                    wasRegionInvalid = false;

                    EmptyCellsCount = elements.SelectMany(l => l).Count(c => string.IsNullOrEmpty(c.Text));
                    IsValid = validityMap.SelectMany(list => list).All(validity => validity);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
