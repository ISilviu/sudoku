﻿using System;
using System.Collections.Generic;

namespace Sudoku.Domain.Data
{
    using Bounds = Tuple<int, int>;
    public static class BoardRegionsToBounds
    {
        public static readonly Dictionary<int, Tuple<Bounds, Bounds>> fourByFour = new Dictionary<int, Tuple<Bounds, Bounds>>(4)
        {
            { 1, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(0, 1)) },
            { 2, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(2, 3)) },

            { 3, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(0, 1)) },
            { 4, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(2, 3)) },
        };

        public static readonly Dictionary<int, Tuple<Bounds, Bounds>> sixBySix = new Dictionary<int, Tuple<Bounds, Bounds>>(4)
        {
            { 1, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(0, 2)) },
            { 2, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(3, 5)) },

            { 3, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(0, 2)) },
            { 4, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(3, 5)) },

            { 5, new Tuple<Bounds, Bounds>(new Bounds(4, 5), new Bounds(0, 2)) },
            { 6, new Tuple<Bounds, Bounds>(new Bounds(4, 5), new Bounds(3, 5)) },
        };

        public static readonly Dictionary<int, Tuple<Bounds, Bounds>> eightByEight = new Dictionary<int, Tuple<Bounds, Bounds>>(4)
        {
            { 1, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(0, 3)) },
            { 2, new Tuple<Bounds, Bounds>(new Bounds(0, 1), new Bounds(4, 7)) },

            { 3, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(0, 3)) },
            { 4, new Tuple<Bounds, Bounds>(new Bounds(2, 3), new Bounds(4, 7)) },

            { 5, new Tuple<Bounds, Bounds>(new Bounds(4, 5), new Bounds(0, 3)) },
            { 6, new Tuple<Bounds, Bounds>(new Bounds(4, 5), new Bounds(4, 7)) },

            { 7, new Tuple<Bounds, Bounds>(new Bounds(6, 7), new Bounds(0, 3)) },
            { 8, new Tuple<Bounds, Bounds>(new Bounds(6, 7), new Bounds(4, 7)) },
        };

        public static readonly Dictionary<int, Tuple<Bounds, Bounds>> nineByNine = new Dictionary<int, Tuple<Bounds, Bounds>>(4)
        {
            { 1, new Tuple<Bounds, Bounds>(new Bounds(0, 2), new Bounds(0, 2)) },
            { 2, new Tuple<Bounds, Bounds>(new Bounds(0, 2), new Bounds(3, 5)) },
            { 3, new Tuple<Bounds, Bounds>(new Bounds(0, 2), new Bounds(6, 8)) },

            { 4, new Tuple<Bounds, Bounds>(new Bounds(3, 5), new Bounds(0, 2)) },
            { 5, new Tuple<Bounds, Bounds>(new Bounds(3, 5), new Bounds(3, 5)) },
            { 6, new Tuple<Bounds, Bounds>(new Bounds(3, 5), new Bounds(6, 8)) },

            { 7, new Tuple<Bounds, Bounds>(new Bounds(6, 8), new Bounds(0, 2)) },
            { 8, new Tuple<Bounds, Bounds>(new Bounds(6, 8), new Bounds(3, 5)) },
            { 9, new Tuple<Bounds, Bounds>(new Bounds(6, 8), new Bounds(6, 8)) },
        };
    }
}
