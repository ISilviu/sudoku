﻿using System;
using System.Collections.Generic;

namespace Sudoku.Domain.Data
{
    using Size = Tuple<int, int>;
    using Bounds = Tuple<int, int>;
    public static class BoardSizeToRegions
    {
        public static readonly Dictionary<Size, Dictionary<int, Tuple<Bounds, Bounds>>> values = new Dictionary<Size, Dictionary<int, Tuple<Size, Size>>>(4)
        {
            {new Size(4, 4), BoardRegionsToBounds.fourByFour },
            {new Size(6, 6), BoardRegionsToBounds.sixBySix },
            {new Size(8, 8), BoardRegionsToBounds.eightByEight },
            {new Size(9, 9), BoardRegionsToBounds.nineByNine },
        };
    }
}
