﻿using System;
using System.Collections.Generic;

namespace Sudoku.Domain.Data
{
    using Position = Tuple<int, int>;
    using Size = Tuple<int, int>;
    public static class BoardSizeToPositions
    {
        public static readonly Dictionary<Size, Dictionary<Position, int>> values = new Dictionary<Position, Dictionary<Position, int>>(4)
        {
            {new Size(4, 4), BoardPositionsToRegions.fourByFour },
            {new Size(6, 6), BoardPositionsToRegions.sixBySix },
            {new Size(8, 8), BoardPositionsToRegions.eightByEight },
            {new Size(9, 9), BoardPositionsToRegions.nineByNine },
        };
    }
}
