﻿using System;
using System.Collections.Generic;

namespace Sudoku.Domain.Data
{
    using Position = Tuple<int, int>;
    public static class BoardPositionsToRegions
    {
        #region FourByFour
        public static readonly Dictionary<Position, int> fourByFour = new Dictionary<Position, int>(16)
        {
            {new Position(0, 0), 1 },
            {new Position(0, 1), 1 },
            {new Position(1, 0), 1 },
            {new Position(1, 1), 1 },

            {new Position(0, 2), 2 },
            {new Position(0, 3), 2 },
            {new Position(1, 3), 2 },
            {new Position(1, 2), 2 },

            {new Position(2, 0), 3 },
            {new Position(3, 0), 3 },
            {new Position(2, 1), 3 },
            {new Position(3, 1), 3 },

            {new Position(2, 2), 4 },
            {new Position(2, 3), 4 },
            {new Position(3, 2), 4 },
            {new Position(3, 3), 4 },
        };
        #endregion

        #region SixBySix
        public static readonly Dictionary<Position, int> sixBySix = new Dictionary<Position, int>(36)
        {
            {new Position(0, 0), 1 },
            {new Position(0, 1), 1 },
            {new Position(1, 0), 1 },
            {new Position(1, 1), 1 },
            {new Position(0, 2), 1 },
            {new Position(1, 2), 1 },

            {new Position(0, 3), 2 },
            {new Position(1, 3), 2 },
            {new Position(0, 4), 2 },
            {new Position(1, 4), 2 },
            {new Position(0, 5), 2 },
            {new Position(1, 5), 2 },

            {new Position(2, 0), 3 },
            {new Position(3, 0), 3 },
            {new Position(2, 1), 3 },
            {new Position(3, 1), 3 },
            {new Position(2, 2), 3 },
            {new Position(3, 2), 3 },

            {new Position(2, 3), 4 },
            {new Position(3, 3), 4 },
            {new Position(2, 4), 4 },
            {new Position(3, 4), 4 },
            {new Position(2, 5), 4 },
            {new Position(3, 5), 4 },

            {new Position(4, 0), 5 },
            {new Position(5, 0), 5 },
            {new Position(4, 1), 5 },
            {new Position(5, 1), 5 },
            {new Position(4, 2), 5 },
            {new Position(5, 2), 5 },

            {new Position(4, 3), 6 },
            {new Position(5, 3), 6 },
            {new Position(4, 4), 6 },
            {new Position(5, 4), 6 },
            {new Position(4, 5), 6 },
            {new Position(5, 5), 6 },
        };
        #endregion

        #region EightByEight
        public static readonly Dictionary<Position, int> eightByEight = new Dictionary<Position, int>(64)
        {
            {new Position(0, 0), 1 },
            {new Position(0, 1), 1 },
            {new Position(1, 0), 1 },
            {new Position(1, 1), 1 },
            {new Position(0, 2), 1 },
            {new Position(1, 2), 1 },
            {new Position(0, 3), 1 },
            {new Position(1, 3), 1 },

            {new Position(0, 4), 2 },
            {new Position(1, 4), 2 },
            {new Position(0, 5), 2 },
            {new Position(1, 5), 2 },
            {new Position(0, 6), 2 },
            {new Position(1, 6), 2 },
            {new Position(0, 7), 2 },
            {new Position(1, 7), 2 },

            {new Position(2, 0), 3 },
            {new Position(3, 0), 3 },
            {new Position(2, 1), 3 },
            {new Position(3, 1), 3 },
            {new Position(2, 2), 3 },
            {new Position(3, 2), 3 },
            {new Position(2, 3), 3 },
            {new Position(3, 3), 3 },

            {new Position(2, 4), 4 },
            {new Position(3, 4), 4 },
            {new Position(2, 5), 4 },
            {new Position(3, 5), 4 },
            {new Position(2, 6), 4 },
            {new Position(3, 6), 4 },
            {new Position(2, 7), 4 },
            {new Position(3, 7), 4 },


            {new Position(4, 0), 5 },
            {new Position(5, 0), 5 },
            {new Position(4, 1), 5 },
            {new Position(5, 1), 5 },
            {new Position(4, 2), 5 },
            {new Position(5, 2), 5 },
            {new Position(4, 3), 5 },
            {new Position(5, 3), 5 },

            {new Position(4, 4), 6 },
            {new Position(5, 4), 6 },
            {new Position(4, 5), 6 },
            {new Position(5, 5), 6 },
            {new Position(4, 6), 6 },
            {new Position(5, 6), 6 },
            {new Position(4, 7), 6 },
            {new Position(5, 7), 6 },

            {new Position(6, 0), 7 },
            {new Position(7, 0), 7 },
            {new Position(6, 1), 7 },
            {new Position(7, 1), 7 },
            {new Position(6, 2), 7 },
            {new Position(7, 2), 7 },
            {new Position(6, 3), 7 },
            {new Position(7, 3), 7 },

            {new Position(6, 4), 8 },
            {new Position(7, 4), 8 },
            {new Position(6, 5), 8 },
            {new Position(7, 5), 8 },
            {new Position(6, 6), 8 },
            {new Position(7, 6), 8 },
            {new Position(6, 7), 8 },
            {new Position(7, 7), 8 },
        };
        #endregion

        #region NineByNine
        public static readonly Dictionary<Position, int> nineByNine = new Dictionary<Position, int>(81)
        {
            {new Position(0, 0), 1 },
            {new Position(1, 0), 1 },
            {new Position(2, 0), 1 },
            {new Position(0, 1), 1 },
            {new Position(1, 1), 1 },
            {new Position(2, 1), 1 },
            {new Position(0, 2), 1 },
            {new Position(1, 2), 1 },
            {new Position(2, 2), 1 },

            {new Position(0, 3), 2 },
            {new Position(1, 3), 2 },
            {new Position(2, 3), 2 },
            {new Position(0, 4), 2 },
            {new Position(1, 4), 2 },
            {new Position(2, 4), 2 },
            {new Position(0, 5), 2 },
            {new Position(1, 5), 2 },
            {new Position(2, 5), 2 },

            {new Position(0, 6), 3 },
            {new Position(1, 6), 3 },
            {new Position(2, 6), 3 },
            {new Position(0, 7), 3 },
            {new Position(1, 7), 3 },
            {new Position(2, 7), 3 },
            {new Position(0, 8), 3 },
            {new Position(1, 8), 3 },
            {new Position(2, 8), 3 },

            {new Position(3, 0), 4 },
            {new Position(4, 0), 4 },
            {new Position(5, 0), 4 },
            {new Position(3, 1), 4 },
            {new Position(4, 1), 4 },
            {new Position(5, 1), 4 },
            {new Position(3, 2), 4 },
            {new Position(4, 2), 4 },
            {new Position(5, 2), 4 },

            {new Position(3, 3), 5 },
            {new Position(4, 3), 5 },
            {new Position(5, 3), 5 },
            {new Position(3, 4), 5 },
            {new Position(4, 4), 5 },
            {new Position(5, 4), 5 },
            {new Position(3, 5), 5 },
            {new Position(4, 5), 5 },
            {new Position(5, 5), 5 },

            {new Position(3, 6), 6 },
            {new Position(4, 6), 6 },
            {new Position(5, 6), 6 },
            {new Position(3, 7), 6 },
            {new Position(4, 7), 6 },
            {new Position(5, 7), 6 },
            {new Position(3, 8), 6 },
            {new Position(4, 8), 6 },
            {new Position(5, 8), 6 },

            {new Position(6, 0), 7 },
            {new Position(7, 0), 7 },
            {new Position(8, 0), 7 },
            {new Position(6, 1), 7 },
            {new Position(7, 1), 7 },
            {new Position(8, 1), 7 },
            {new Position(6, 2), 7 },
            {new Position(7, 2), 7 },
            {new Position(8, 2), 7 },

            {new Position(6, 3), 8 },
            {new Position(7, 3), 8 },
            {new Position(8, 3), 8 },
            {new Position(6, 4), 8 },
            {new Position(7, 4), 8 },
            {new Position(8, 4), 8 },
            {new Position(6, 5), 8 },
            {new Position(7, 5), 8 },
            {new Position(8, 5), 8 },

            {new Position(6, 6), 9 },
            {new Position(7, 6), 9 },
            {new Position(8, 6), 9 },
            {new Position(6, 7), 9 },
            {new Position(7, 7), 9 },
            {new Position(8, 7), 9 },
            {new Position(6, 8), 9 },
            {new Position(7, 8), 9 },
            {new Position(8, 8), 9 },
        };
        #endregion
    }
}
