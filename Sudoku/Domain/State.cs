﻿using Newtonsoft.Json;

namespace Sudoku.Domain
{
    public class State
    {
        public Board GameBoard { get; }
        public int UserAllocatedTime { get; }

        [JsonConstructor]
        public State(Board gameBoard, int userAllocatedTime)
        {
            GameBoard = gameBoard;
            UserAllocatedTime = userAllocatedTime;
        }
    }
}
