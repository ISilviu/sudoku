﻿using System;
using System.ComponentModel;

namespace Sudoku.Domain
{
    public class Cell : INotifyPropertyChanged
    {
        public Tuple<int, int> RowAndColumn { get; private set; }

        private bool isReadOnly;
        public bool IsReadOnly
        {
            get => isReadOnly;
            set
            {
                isReadOnly = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        private string text;
        public string Text
        {
            get => text;

            set
            {
                text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        private int number;
        public int Number
        {
            get => number;
            set
            {
                number = value;
                OnPropertyChanged(nameof(Number));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Cell(Tuple<int, int> rowAndColumn, string text, int number, bool isReadOnly)
        {
            RowAndColumn = rowAndColumn;
            IsReadOnly = isReadOnly;
            Text = text;
            Number = number;
        }
    }
}
