﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Sudoku.Utilities.FileIO
{
    class IOServices
    {
        public static IEnumerable<BitmapImage> ReadImages(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            IEnumerable<FileInfo> files = null;

            try
            {
                files = directoryInfo.EnumerateFiles();
            }
            catch (DirectoryNotFoundException)
            {
                files = null;
            }
            catch (SecurityException)
            {
                files = null;
            }


            if (files != null)
            {
                foreach (var file in files)
                    yield return new BitmapImage(new Uri(file.FullName, UriKind.Absolute));
            }
            else
                yield return null;
        }

        public static T Deserialize<T>(string path)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static async Task SerializeAsync<T>(T value, string path)
        {
            var serialized = JsonConvert.SerializeObject(value, Formatting.Indented);
            try
            {
                using (var writer = new StreamWriter(path, false))
                    await writer.WriteAsync(serialized);                
            }
            catch (Exception)
            {

            }
        }

        public static async Task<T> DeserializeAsync<T>(string path)
        {
            try
            {
                using (var reader = new StreamReader(path))
                {
                    var data = await reader.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<T>(data);
                }
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        private static async Task<string> ReadAsync(string path)
        {
            try
            {
                using (var reader = new StreamReader(path, false))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception)
            { }
        }

        public static async Task<int[,]> ReadBoardAsync(string path, Tuple<int, int> boardSize)
        {
            try
            {
                string data = null;
                using (var reader = new StreamReader(path))
                    data = await reader.ReadToEndAsync();

                if (data != null)
                {
                    int i = 0, j = 0;
                    int[,] result = new int[boardSize.Item1, boardSize.Item2];
                    foreach (var row in data.Split('\n'))
                    {
                        j = 0;
                        foreach (var col in row.Trim().Split(' '))
                        {
                            result[i, j] = int.Parse(col.Trim());
                            ++j;
                        }
                        ++i;
                    }

                    return result;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<int[,]> GetRandomBoard(string path, Tuple<int, int> boardSize)
        {
            try
            {
                var directoryInfo = new DirectoryInfo(path);
                var files = directoryInfo.GetFiles();

                var random = new Random();
                var randomIndex = random.Next(files.Length);

                var randomFile = files[randomIndex];

                return await ReadBoardAsync(randomFile.FullName, boardSize);
            }
            catch (Exception)
            {
                return null;
            }
        }     
    }
}
