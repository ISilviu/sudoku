﻿using System.Windows.Input;

namespace Sudoku.Commands
{
    class LoginWindow
    {
        public static readonly RoutedUICommand NewUser = new RoutedUICommand("NewUser", "NewUser", typeof(LoginWindow));
        public static readonly RoutedUICommand DeleteUser = new RoutedUICommand("DeleteUser", "DeleteUser", typeof(LoginWindow));
        public static readonly RoutedUICommand StartGame = new RoutedUICommand("StartGame", "StartGame", typeof(LoginWindow));
    }
}
