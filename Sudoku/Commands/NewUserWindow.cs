﻿using System.Windows.Input;

namespace Sudoku.Commands
{
    class NewUserWindow
    {
        public static readonly RoutedUICommand OK = new RoutedUICommand("OK", "OK", typeof(NewUserWindow));
        public static readonly RoutedUICommand ImageLeft = new RoutedUICommand("ImageLeft", "ImageLeft", typeof(NewUserWindow));
        public static readonly RoutedUICommand ImageRight = new RoutedUICommand("ImageRight", "ImageRight", typeof(NewUserWindow));
    }
}
