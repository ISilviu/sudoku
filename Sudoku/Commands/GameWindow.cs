﻿using System.Windows.Input;

namespace Sudoku.Commands
{
    class GameWindow
    {
        public static readonly RoutedUICommand BoardPreview = new RoutedUICommand("BoardPreview", "BoardPreview", typeof(GameWindow));
        public static readonly RoutedUICommand SaveGame = new RoutedUICommand("SaveGame", "SaveGame", typeof(GameWindow));
    }
}
