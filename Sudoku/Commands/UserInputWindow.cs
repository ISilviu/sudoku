﻿using System.Windows.Input;

namespace Sudoku.Commands
{
    class UserInputWindow
    {
        public static readonly RoutedUICommand AddContent = new RoutedUICommand("AddContent", "AddContent", typeof(UserInputWindow));
    }
}
